# jp.kde.org

Japan KDE Users Group Website
日本 KDE ユーザ会 Web サイト

## ローカル開発環境のセットアップ ― Ruby 環境で開発する場合

### 環境要件

- Ruby 2.4.0 以上 ([最新の Jekyll がサポートする Ruby](https://jekyllrb.com/docs/installation/#requirements))
- [docker-compose](https://docs.docker.com/compose/install/) (リダイレクトに関してテストをする場合のみ)

### `jekyll serve` コマンドによるテスト

このリポジトリーのルートディレクトリに移動し、`bundle install` で依存パッケージをインストールした後、`jekyll serve` コマンドで開発用サーバーを起動して下さい。通常は http://127.0.0.1:4000/ でアクセスできるようになるはずです。

```shell
$ cd /path/to/jp.kde.org/
$ bundle install
$ bundle exec jekyll serve
```

### `docker-compose` によるテスト

.htaccess のテストを行うためには、docker-compose で Apache 環境を立ち上げる必要があります。

このリポジトリーのルートディレクトリーに移動し、`bundle install` で依存パッケージをインストールした後、`jekyll build` コマンドでビルドを行って下さい。その後、`docker-compose up` を実行することで、Apache の環境が起動します。Docker コンテナの外側から、http://localhost:4000 にアクセスすることで、ページを表示することができます。

```shell
$ cd /path/to/jp.kde.org/
$ bundle install
$ bundle exec jekyll build
$ docker-compose up
```

## ステージング環境

Netlify に作成しています。

https://jp-kde-org-preview.netlify.com/

現在のところ[小川](https://gitlab.com/phanect)のみがアップロードできるようになっていますが、アップロード権限の付け方は今後検討します。
