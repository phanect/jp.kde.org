KDE セキュリティアドバイザリ: Okular の PDB 処理によるメモリ破損の脆弱性
初版発行日: 2010-08-25
URL: http://www.kde.org/info/security/advisory-20100825-1.txt

0. 参考情報:
    CVE-2010-2575
    SA40952

1. 影響を受けるシステム:
    KDE SC 4.3.0 から KDE SC 4.5.0 までに含まれる Okular。
    これより古いバージョンの KDE SC も影響を受ける可能性があります。

2. 概要:
    この脆弱性は、細工された PDB ファイルをユーザが閲覧させられるな
    どして、ヒープベースのバッファオーバーフローによる攻撃を受けた PDB
    ファイルに埋め込まれた画像を処理する際に、
    generators/plucker/inplug/image.cpp 内にある
    "TranscribePalmImageToJPEG()" 関数で RLE 展開をしている間に発
    生する境界誤差に起因します。
    この脆弱性は Secunia Research の Stefan Cornelius によって発見されま
    した。上記の文も Stefan Cornelius が提供してくれました。

3. 影響:

    攻撃により、Okular がクラッシュしたり、任意のコードが実行される可能
    性があります。

4. 解決策:

    これらの脆弱性を修正するソースコードパッチが利用可能です。このアドバ
    イザリを書いている頃には、多くの OS ベンダやバイナリパッケージ提供者
    がバイナリパッケージを更新しているでしょう。更新されたバイナリ
    パッケージの取得方法については、お使いの OS のベンダや、バイナリ
    パッケージ提供者にお問い合せ下さい。

5. パッチ:

    この問題についてのパッチは Albert Astals Cid (aacid@kde.org) が提供
    しました。

    パッチは KDE の Subversion リポジトリに、以下のバージョン番号で
    コミットされています。

    4.3 branch: r1167825
    4.4 branch: r1167826
    4.5 branch: r1167827
    Trunk: r1167828

    KDE SC 4.3、KDE SC 4.4、KDE SC 4.5 のパッチは、以下のコマンドと、
    SHA1 チェックサム参照で Subversion のディレクトリから直接取得できるはず
    です。(チェックアウトの必要はありません)

注意: これらの SHA1 チェックサムは、以下の "svn diff" コマンドの出力で、 メタデータの出力を含んでいます。:

    4.3 branch: f1ad2e50ce0ce8592c767365b87a22a80943aa28
    svn diff -r 1167824:1167825 \
    svn://anonsvn.kde.org/home/kde/branches/KDE/4.3/kdegraphics

    4.4 branch: 13f06704919f239ef29ff63e6c1ddf8fa162af9c
    svn diff -r 1167825:1167826 \
    svn://anonsvn.kde.org/home/kde/branches/KDE/4.4/kdegraphics

    4.5 branch: d739c58873599f7324c9d6500d3615f803bff39e
    svn diff -r 1167826:1167827 \
    svn://anonsvn.kde.org/home/kde/branches/KDE/4.5/kdegraphics 
